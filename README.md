# 005_006_asyncio_treading_multiprocessing_GIL

# Поєднання двох лекцій:
# Асінхронне програмування у Python
# Багатопоточне програмування у Python

---

[video - 1](https://youtu.be/BdGoNMYjZbQ) - IO-bound - thread, async, multiprocessing

[video - 2](https://youtu.be/bLC5YTutRO4) - CPU-bound - multiprocessing, GIL

### программа навчання: **Python Advanced 2022**

### номер заняття: 5, 6

### засоби навчання: Python; інтегроване середовище розробки (PyCharm або Microsoft Visual Studio + Python Tools for Visual Studio + можливість використання Юпітер Jupyter Notebook)

---

### Огляд, мета та призначення уроку



**Вивчивши матеріал даного заняття, учень зможе:**


**Зміст уроку:**
